package akkaiot

import akka.actor.{ Props, Actor, ActorLogging }
import java.util.concurrent.ThreadLocalRandom

object WorkProcessor {
  def props(): Props = Props(new WorkProcessor)

  case class DeviceStateSetting(deviceType: String, state: Double, setting: Double)
}


class WorkProcessor extends Actor with ActorLogging {
  import WorkProcessor._
  def random = ThreadLocalRandom.current

  def receive = {
    case work @ Work(workId, deviceType, deviceId, state, setting) => {
      val newStateSetting: DeviceStateSetting = deviceType match {
        case "temperature" =>
          nextDeviceStateSetting(work, Map(4.00->"COLD", 19.00->"WARM", 27.00->"HOT"), "temperature", (-39.99, 39.99), (-2.00, 2.00))

        case "humidity" => 
            nextDeviceStateSetting(work, Map(53.00->"DRY AIR", 62.00->"NORMAL HUMIDIY", 70.00->"VERY HUMID"), "humidity", (55.00, 75.00), (-2.00, 2.00)) 

        case "pressure" => 
            nextDeviceStateSetting(work, Map(101325.00->"NORMAL ATMOSPHERIC PRESSURE", 101200.00->"AVERAGE ATMOSPHERIC PRESSURE", 101600.00->"ABOVE AVERAGE ATMOSPHERIC PRESSURE"), "atmospheric pressure", (101423.88, 101625.112), (-150.00, 150.00))     
        
        case "dew-point" => 
          nextDeviceStateSetting(work, Map(9.00->"VERY LOW", 15.00->"NORMAL", 20.00->"HIGH"), "dew point", (8.00, 22.00), (-1.00, 1.00))

        case "wind-speed" => 
          nextDeviceStateSetting(work, Map(10.00->"RELATIVELY WINDY", 15.00->"VERY WINDY" , 25.00->"STRONG WINDS"), "wind velocity", (10.00, 35.00), (-2.00, 2.00))

        case _ =>
          log.info("Work Processor -> ALERT: Unknown Device {}-{}", work.deviceType, work.deviceId)
          DeviceStateSetting(deviceType, state, setting)
      }

      val result = WorkResult(workId, deviceType, deviceId, newStateSetting.state, newStateSetting.setting)
      sender() ! Worker.WorkProcessed(result)
    }

    case _ =>
      log.info("Work Processor -> ALERT: Received unknown message!")
  }

  def nextDeviceStateSetting(
      work: Work, stateMap: Map[Double, String], settingType: String, settingLimit: (Double, Double), changeLimit: (Double, Double)
    ): DeviceStateSetting = {

    val nextState = random.nextDouble(0.00, stateMap.size)

    val nextStateText = if (nextState == work.currState) "Keep state " + stateMap(work.currState) else
      "Switch to " + stateMap(nextState)

    val randomChange = random.nextDouble(changeLimit._1, changeLimit._2 + 1)
    val randomSetting = work.currSetting + randomChange

    val nextSettingChange = if (randomChange == 0) 0 else {
      if (randomSetting < settingLimit._1 || randomSetting > settingLimit._2) 0 else randomChange
    }
    val nextSetting = work.currSetting + nextSettingChange

    val nextSettingText = if (nextSettingChange == 0) s"NO $settingType change" else {
      if (nextSettingChange > 0) s"RAISE $settingType by $nextSettingChange" else
        s"LOWER $settingType by $nextSettingChange"
    }

    log.info("Work Processor -> {}-{}: {} | {}", work.deviceType, work.deviceId, nextStateText, nextSettingText)
    DeviceStateSetting(work.deviceType, nextState, nextSetting)
  }
}