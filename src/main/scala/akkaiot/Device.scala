package akkaiot

import java.util.UUID
import java.util.concurrent.ThreadLocalRandom
import scala.concurrent.duration._
import scala.math.exp
import akka.actor.{ Props, ActorRef, Actor, ActorLogging }

import com.sandinh.paho.akka._
import com.sandinh.paho.akka.MqttPubSub._

object Device {
  def props(deviceType: String, deviceId: String, mqttPubSub: ActorRef): Props = Props(new Device(deviceType, deviceId, mqttPubSub))

  case object Tick
}

class Device(deviceType: String, deviceId: String, mqttPubSub: ActorRef) extends Actor with ActorLogging {

  import Device._
  import context.dispatcher

  /**
   * Variables : Temperature , Humidiy,  Pressure ,  Dew  Point, Wind Speed
   * States: Temp : -+40°C , Humidity: formulated , Pressure 700 1200 millibars, Dew Point formulated , Wind Speed  R : 98.256 107.348km : Coriolis force
   */
  private var opState: Double = 0.00  
  private var setting: Double = 0.00  

  def scheduler = context.system.scheduler
  def random = ThreadLocalRandom.current
  def nextWorkId(): String = UUID.randomUUID().toString

  override def preStart(): Unit = {
      val temp = random.nextDouble(-39.99, 39.99) 
      val relhum = random.nextDouble(55.00, 75.00) // Get anything between 75% and 55%
      val dp = (100.00 - relhum) / 5  
      val hum = 100.00 * ((611 * exp(5423 * ((1 / 273)) - (1 / (dp + 273)))) / (611 * exp(5423 * ((1 / 273)-(1 / temp))))) // Clausius-Clapeyron 
      val ws = ((0.0001) * random.nextDouble(98.256, 107.348)) // R = v/f : f = 10^-4 Coriolis Force
      val press = 101325 * exp(((0.00 - 9.81) * 0.0289644 * (200))/(8.31444598 * (temp + 273))) //  Barometric formula
    opState = deviceType match {
      case "temperature" => temp
      case "humidity" => hum
      case "pressure" => press
      case "dew-point" => dp
      case "wind-speed" => ws
      case _ => 0
    }

    setting = deviceType match {
      case "temperature" => random.nextDouble(23.51, 28.22)
      case "humidity" => random.nextDouble(55.28, 61.87)
      case "pressure" => random.nextDouble(101423.88, 101625.112)
      case "dew-point" => random.nextDouble(16.86, 19.622)
      case "wind-speed" => random.nextDouble(10.00, 15.00)
      case _ => 0
    }

    scheduler.scheduleOnce(5.seconds, self, Tick)
    log.info("Device -> {}-{} started", deviceType, deviceId)
  }

  override def postRestart(reason: Throwable): Unit = ()

  override def postStop(): Unit = log.info("Device -> {}-{} stopped.", deviceType, deviceId)

  def receive = {
    case Tick => {
      val workId = nextWorkId()
      val work = Work(workId, deviceType, deviceId, opState, setting)
      log.info("Device -> {}-{} with state {} created work (Id: {}) ", deviceType, deviceId, opState, workId)

      val payload = MqttConfig.writeToByteArray(work)
      log.info("Device -> Publishing MQTT Topic {}: Device {}-{}", MqttConfig.topic , deviceType, deviceId)

      mqttPubSub ! new Publish(MqttConfig.topic, payload)

      context.become(waitAccepted(work, payload), discardOld = false)
    }

    case WorkResult(workId, deviceType, deviceId, nextState, nextSetting) => {
      log.info("Device -> {}-{} received work result with work Id {}.", deviceType, deviceId, workId)

      opState = nextState
      setting = nextSetting

      log.info("Device -> Updated {}-{} with state {} and setting {}.", deviceType, deviceId, opState, setting)
    }
  }

  def waitAccepted(work: Work, payload: Array[Byte]): Receive = {
    case IotManager.Ok(_) =>
      log.info("Device -> Work for {}-{} accepted | Work Id {}", work.deviceType, work.deviceId, work.workId)
      context.unbecome()
      scheduler.scheduleOnce(random.nextInt(3, 10).seconds, self, Tick)

    case IotManager.NotOk(_) =>
      log.info("Device -> ALERT: Work from {}-{} NOT ACCEPTED | Work Id {} | Retrying ... ", work.deviceType, work.deviceId, work.workId)
      scheduler.scheduleOnce(3.seconds, mqttPubSub, new Publish(MqttConfig.topic, payload))
  }
}
