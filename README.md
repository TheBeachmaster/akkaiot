# akkaiot

Akka IoT device manager example 
 
## Console    
> All commnds are done in separate windows 

> Running single instance  


### Start redis  

```bash 
  redis-server redis.conf  
  sbt "runMain akkaiot.Main numOfDevices" 
``` 

 - replace `numOfDevices` with an actual number of devices you'd like to simulate. Default is `3500`  
 
> Running multiple instances  


### Start redis 

```bash  

redis-server redis.conf  

``` 

### Start a persistence master cluster 

```bash 
sbt "runMain akkaiot.Main 2551" 
``` 

### Start a master cluster seed node 

```bash 
sbt "runMain akkaiot.Main 2552" 
``` 

### Launch an IoT node  of `n` devices 

```bash 
sbt "runMain akkaiot.Main 3001 numOfDevices"      
``` 
- replace `numOfDevices` with an actual number of devices you'd like to simulate. Default is `3500`
 
### Launch worker nodes 

```bash 
sbt "runMain akkaiot.Main 0" 
``` 

- Launch multiple worker nodes by running step `5` multiple times 


## Docker 

[WIP]
