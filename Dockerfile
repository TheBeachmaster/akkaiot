FROM openjdk:8


RUN  apt-get update \
  && apt-get install -y \
    apt-transport-https bc apt-utils \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /var/tmp/*  &&\
  echo "deb https://dl.bintray.com/sbt/debian /" | tee -a /etc/apt/sources.list.d/sbt.list  && \
  apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2EE0EA64E40A89B84B2DF73499E82A75642AC823 && \
  apt-get update && \ 
  apt-get install -y sbt

WORKDIR /usr/src/app

COPY . .

CMD sbt "runMain akkaiot.Main 500" 