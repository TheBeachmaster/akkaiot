package akkaiot

object MasterWorkerProtocol {
    case class RegisterWorker(workerId: String)
    case class WorkerRequestsWork(workerId: String)
    case class WorkIsDone(workerId: String, workId: String, result: WorkResult)
    case class WorkFailed(workerId: String, workId: String)

    case object WorkIsReady
    case class Ack(id: String)
}