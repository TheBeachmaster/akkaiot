package akkaiot

import java.io.Serializable

case class Work(workId: String, deviceType: String, deviceId: String, currState: Double, currSetting: Double) extends Serializable

case class WorkResult(workId: String, deviceType: String, deviceId: String, nextState:Double , nextSetting: Double) extends Serializable